from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'buses_auth'
