# Generated by Django 2.0.6 on 2018-06-18 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buses', '0006_auto_20180618_1738'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='img',
            field=models.ImageField(null=True, upload_to='profile_pics'),
        ),
    ]
