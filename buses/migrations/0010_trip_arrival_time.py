# Generated by Django 2.0.6 on 2018-06-19 03:44

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('buses', '0009_auto_20180619_0307'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='arrival_time',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Horario de llegada'),
        ),
    ]
