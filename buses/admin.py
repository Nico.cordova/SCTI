from django.contrib import admin
from buses.models import Bus, Trip, CrewMember, Stop, Passanger, PassangerTicket
from django.utils.safestring import mark_safe


@admin.register(Bus)
class BusAdmin(admin.ModelAdmin):
    list_display = ('model', 'patent', 'kms', 'seats')
    
@admin.register(CrewMember)
class CrewMemberAdmin(admin.ModelAdmin):
    list_display = ('name','last_name', 'rut', 'email','phone','role')

@admin.register(Trip)
class TripAdmin(admin.ModelAdmin):
    list_display = ('bus', 'source', 'destination', 'start_time','bus_driver')

@admin.register(PassangerTicket)
class PassangerTicketAdmin(admin.ModelAdmin):
    list_display = ('seat', 'trip')

@admin.register(Passanger)
class PassangerAdmin(admin.ModelAdmin):
    list_display = ('name', 'rut', 'dv', 'email','phone')

@admin.register(Stop)
class StopAdmin(admin.ModelAdmin):
    list_display = ('location', 'arrival', 'leave')

