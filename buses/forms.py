from django import forms
from buses.models import Bus,Trip,Passanger,CrewMember



class BusForm(forms.ModelForm):
    class Meta:
        model = Bus
        fields = ['model', 'patent', 'kms', 'seats']

class CrewMemberForm(forms.ModelForm):
    class Meta:
        model = CrewMember
        fields = ['name', 'last_name','img','phone', 'email','rut','dv','role']

class TripForm(forms.ModelForm):
    class Meta:
        model = Trip
        fields = ['bus', 'source', 'destination', 'start_time','bus_driver']
        widgets = {
            'start_time': forms.DateInput(attrs={'class':'datepicker'}),
        }