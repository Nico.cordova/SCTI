# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
# Create your models here.
from buses.defines import CREW_TYPE_CHOICES
@python_2_unicode_compatible
class CrewMember(models.Model):
    user = models.OneToOneField(User,null=True,default=None,on_delete=models.SET_NULL)    
    rut = models.IntegerField(default=0,blank=True,null=True,verbose_name="Rut")
    dv = models.CharField(max_length=1,default="",blank=True,null=True,verbose_name="Dígito Verificador")
    email = models.CharField(max_length=150,default="",verbose_name='Email')
    phone = models.IntegerField(default=0,blank=True,null=True,verbose_name='Teléfono')
    name = models.CharField(max_length=100,verbose_name='Nombre')
    last_name = models.CharField(max_length=100,verbose_name='Apellido')
    img = models.ImageField(upload_to="profile_pics",default=None,verbose_name='Imagen de Perfíl')
    role = models.CharField(max_length=2,choices=CREW_TYPE_CHOICES,default="CO",verbose_name="Rol")
    def __str__(self):
        return "%s %s %d-%s" % (self.name,self.last_name,self.rut,self.dv)

class Bus(models.Model):
    model = models.CharField(max_length=100,verbose_name="Modelo")
    patent = models.CharField(max_length=100,verbose_name="Patente")
    kms = models.IntegerField(default=0,verbose_name="Kilometraje")
    seats = models.IntegerField(default=0,verbose_name="Cant. Asientos")

    def __str__(self):
        return "%s - %s" % (self.model,self.patent)

@python_2_unicode_compatible
class Trip(models.Model):
    bus = models.ForeignKey(Bus,null=True,default=None,on_delete=models.CASCADE,verbose_name="Bus")
    asisstant1 = models.ForeignKey(CrewMember,default=None,null=True,on_delete=models.SET_NULL,verbose_name="Asistente de viaje 1",related_name="asistente_1")
    asisstant2 = models.ForeignKey(CrewMember,default=None,null=True,on_delete=models.SET_NULL,verbose_name="Asistente de viaje 2",related_name="asistente_2")
    source = models.CharField(max_length=300,verbose_name='Desde')
    destination = models.CharField(max_length=300,verbose_name='Hasta')
    start_time = models.DateTimeField(default=timezone.now,verbose_name='Hora de partida')
    arrival_time = models.DateTimeField(default=timezone.now,verbose_name="Horario de llegada")
    bus_driver = models.ForeignKey(CrewMember,default=None,null=True,on_delete=models.SET_NULL,verbose_name="Conductor")

    def __str__(self):
        return "De:%s Hasta:%s - %s" % (self.source,self.destination,self.start_time)

class Stop(models.Model):
    trip = models.ForeignKey(Trip,null=True,default=None,on_delete=models.CASCADE)
    location = models.CharField(max_length=200)
    arrival = models.DateTimeField(default=timezone.now)
    leave = models.DateTimeField(default=timezone.now)

class Passanger(models.Model):
    name = models.CharField(max_length=100,default="")
    rut = models.IntegerField(default=0)
    dv = models.CharField(max_length=1,default="")
    email = models.CharField(max_length=150,default="")
    phone = models.IntegerField(default=0)

class PassangerTicket(models.Model):
    seat = models.CharField(max_length=5)
    trip = models.ForeignKey(Trip,default=None,null=True,on_delete=models.CASCADE)
    passanger = models.ForeignKey(Passanger,default=None,null=True,on_delete=models.CASCADE)
    used = models.IntegerField(default=0)
