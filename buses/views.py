# -*- coding: utf-8 -*-
from django.shortcuts import render
from buses.forms import BusForm, CrewMemberForm, TripForm
from buses.models import Bus,Trip,CrewMember,Stop,Passanger
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.utils import timezone
import datetime
from unidecode import unidecode
# Create your views here.
@login_required(login_url='/auth/login')
def index(request):
    data = {
    }
    user_type = "AD"
    print(request.user)
    try:
        if request.user.crewmember.role == "CO":
            user_type = "CO"
        else:
            user_type = "AS"

    except AttributeError:
        pass

    lista = []
    hoy = timezone.now()
    for trip in Trip.objects.all():
        if (trip.arrival_time - hoy).total_seconds() < 0:
            lista.append(trip)

    if len(lista) == 0:
        lista = "False"    
    print("User_type ",user_type)
    data['user_type'] = user_type
    data['list'] = lista
    template_name = 'index.html'
    return render(request,template_name,data)

@login_required(login_url='/auth/login')
def bus_add(request):
    data = {}
    if request.method == "POST":
        data['form'] = BusForm(request.POST)
        if data['form'].is_valid():
            # aca el formulario valido
            data['form'].save()

            return redirect('bus_add')

    else:
        data['form'] = BusForm()

    data["titulo"] = "Agregar"
    template_name = 'buses/add_bus.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def bus_list(request):
    data = {}

    # SELECT * FROM player
    object_list = Bus.objects.all().order_by('-id')

    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')

    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)

    template_name = 'buses/list_bus.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def bus_del(request,pk):
    bus = Bus.objects.get(pk=pk)
    bus.delete()

    return HttpResponseRedirect(reverse("bus_list"))

@login_required(login_url='/auth/login')
def bus_edit(request,pk):
    data = {}
    bus = Bus.objects.get(pk=int(pk))
    if request.method == "POST":
        data['form'] = BusForm(request.POST, request.FILES, instance=bus)
        if data['form'].is_valid():
            # aca el formulario valido
            data['form'].save()
            return redirect('bus_list')

    else:
        print("Este es el Bus ", bus)
        data['form'] = BusForm(instance=bus)

    data["titulo"] = "Editar"
    template_name = 'buses/add_bus.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def crewmember_add(request):
    data = {}
    if request.method == "POST":
        data['form'] = CrewMemberForm(request.POST,request.FILES)
        username = request.POST["rut"]
        password = request.POST["phone"]
        print("Esta esl a password: ",password)
        user = User.objects.create_user(username=username,password=password)
        user.save()
        if data['form'].is_valid():
            # aca el formulario valido
            crewmember=data['form'].save()
            crewmember.user = user
            crewmember.save()
            return redirect('crewmember_add')

    else:
        data['form'] = CrewMemberForm()

    data["titulo"] = "Agregar"
    template_name = 'conductores/add_crewmember.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def crewmember_list(request):
    data = {}

    # SELECT * FROM player
    object_list = CrewMember.objects.all().order_by('-id')

    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')

    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)

    template_name = 'conductores/list_crewmember.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def crewmember_del(request,pk):
    crewmember = CrewMember.objects.get(pk=pk)
    crewmember.delete()

    return HttpResponseRedirect(reverse("crewmember_list"))

@login_required(login_url='/auth/login')
def crewmember_edit(request,pk):
    data = {}
    crewmember = CrewMember.objects.get(pk=int(pk))
    if request.method == "POST":
        data['form'] = CrewMemberForm(request.POST, request.FILES, instance=bus)
        if data['form'].is_valid():
            # aca el formulario valido
            data['form'].save()
            return redirect('crewmember_list')

    else:
        print("Este es el CrewMember ", crewmember)
        data['form'] = CrewMemberForm(instance=crewmember)

    data["titulo"] = "Editar"
    template_name = 'conductores/add_crewmember.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def trip_add(request):
    data = {}
    if request.method == "POST":
        bus = Bus.objects.get(pk=request.POST['buspk'])
        source = request.POST['source']
        destination = request.POST['destination']
        start_time = request.POST['start_time']
        bus_crewmember = CrewMember.objects.get(pk=request.POST['crewmemberpk'])
        return redirect('trip_add')
    else:
        data['buses'] = Bus.objects.all()
        data['conductores'] = CrewMember.objects.filter(role="CO").all()
        data['asistentes'] = CrewMember.objects.filter(role="AS")
    data["titulo"] = "Agregar"
    template_name = 'viajes/add_trip.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def trip_list(request):
    data = {}

    # SELECT * FROM player
    object_list = Trip.objects.all().order_by('-id')

    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')

    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)

    template_name = 'conductores/list_crewmember.html'
    return render(request, template_name, data)

@login_required(login_url='/auth/login')
def trip_del(request,pk):
    trip = Trip.objects.get(pk=pk)
    trip.delete()

    return HttpResponseRedirect(reverse("crewmember_list"))

@login_required(login_url='/auth/login')
def trip_edit(request,pk):
    data = {}
    trip = Trip.objects.get(pk=int(pk))
    if request.method == "POST":
        data['form'] = TripForm(request.POST, request.FILES, instance=trip)
        if data['form'].is_valid():
            # aca el formulario valido
            data['form'].save()
            return redirect('trip_list')

    else:
        print("Este es el trip ", trip)
        data['form'] = TripForm(instance=trip)

    data["titulo"] = "Editar"
    template_name = 'viajes/add_trip.html'
    return render(request, template_name, data)
