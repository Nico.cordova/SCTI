from django.urls import path
from buses import views


urlpatterns = [
    path('bus_add', views.bus_add, name="bus_add"),
    path('bus_edit/<int:pk>/', views.bus_edit, name="bus_edit"),
    path('bus_list', views.bus_list, name="bus_list"),
    path('bus_del/<int:pk>/', views.bus_del, name="bus_delete"),
    path('crewmember_add', views.crewmember_add, name="crewmember_add"),
    path('crewmember_edit/<int:pk>/', views.crewmember_edit, name="crewmember_edit"),
    path('crewmember_list', views.crewmember_list, name="crewmember_list"),
    path('crewmember_del/<int:pk>/', views.crewmember_del, name="crewmember_delete"),
    path('trip_add', views.trip_add, name="trip_add"),
    path('trip_edit/<int:pk>/', views.trip_edit, name="trip_edit"),
    path('trip_list', views.trip_list, name="trip_list"),
    path('trip_del/<int:pk>/', views.trip_del, name="trip_delete"),
]
